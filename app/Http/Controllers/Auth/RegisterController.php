<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Contracts\Validation\Factory as ValidatorFactory;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * @var ValidatorFactory
     */
    private $validatorFactory;

    /**
     * Create a new controller instance.
     *
     * @param EntityManagerInterface $entityManager
     * @param Hasher $hasher
     * @param ValidatorFactory $validatorFactory
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Hasher $hasher,
        ValidatorFactory $validatorFactory)
    {
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;
        $this->validatorFactory = $validatorFactory;

        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return $this->validatorFactory->make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:'. User::class,
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = (new User())
            ->setName($data['name'])
            ->setEmail($data['email'])
            ->setPassword($this->hasher->make($data['password']));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
