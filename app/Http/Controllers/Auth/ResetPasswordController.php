<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * Create a new controller instance.
     *
     * @param EntityManagerInterface $entityManager
     * @param Hasher $hasher
     */
    public function __construct(EntityManagerInterface $entityManager, Hasher $hasher)
    {
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;

        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     *
     * @return void
     */
    protected function resetPassword(CanResetPassword $user, string $password)
    {
        /** @var User $user */
        $user
            ->setPassword($this->hasher->make($password))
            ->setRememberToken(Str::random(60))
        ;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->guard()->login($user);
    }
}
